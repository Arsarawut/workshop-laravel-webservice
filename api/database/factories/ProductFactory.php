<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'subject' => $faker->text(30),
        'title' => $faker->text(100),
        'detail' => $faker->text(2000),
        'price' => $faker->numberBetween(10, 10000),
        'image' => $faker->imageUrl(1280, 720, 'technics') ,
        'status' => "true" ,
    ];
});
