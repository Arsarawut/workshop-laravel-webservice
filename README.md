ติดตั้ง npm install
ติดตั้ง npm install admin

เข้าไปที่ api
composer update

สร้างไฟล์ .env
ก็อปปี้ทุกอย่างใน .env.example แล้ววางใน env
Run php artisan key:generate

สร้าง database 
เลือก utf8mb4_general_ci

แก้ไข ข้อมูล database ที่ .env 
run php artisan serv

run php artisan migrate เพื่อสร้าง database
run php artisan db:seed เพื่อสร้าง ข้อมูล mockup

Generate secret key เพื่อเรียกใช้งาน key
php artisan jwt:secret

เทสโปรแกรม

เข้าไปที่ assets/images/uploads/ ให้สร้างโฟลเดอร์ชื่อว่า source
เทสเพิ่มข้อมูล และ แก้ไขข้อมูล

เข้าไปที่ admin/app/app.js 
ให้แก้ไข ImgBasepath ให้ตรงกับ โฟลเดอร์ที่เราสร้างไว้ xxxxxx
app.constant('ImgBasepath', 'http://localhost/xxxxxxxxx/assets/images/uploads/source/')


****เป็นแค่การติดตั้งคร่าวๆ ทางแอดมิน จะทำวีดีโอมาสอนให้ครับ